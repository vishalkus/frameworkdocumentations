## Custom Prompt Parameters & Description


| Parameter Name      | Description | Required / Optional | Default Value |
| :---        |    :----   | :---: |    :---: |
| headerImageURL | Header Image URL to display image on header of prompt | Required | - |
| bodyText | Body text displayed on prompt | Required  | - |
| bodyBackgroundColor | Body background color in HEX format | Optional | white color |
| mainBackgroundColor | Custom Prompt background color in HEX format | Optional | white color |
| noButtonText | No button text/title | Required | - |
| noButtonTextColor | No button text color in HEX format | Optional | white color |
| noButtonBackgroundColor | No button background color in HEX format | Optional | white color |
| yesButtonText | Yes button text/title | Required | - |
| yesButtonTextColor | Yes button text color in HEX format | Optional | white color |
| yesButtonBackgroundColor | Yes button background color in HEX format | Optional | white color |
| footerBackgroundColor | Footer background color in HEX format | Optional | white color |
| footerText | Footer text displayed in custom prompt footer section, if not passed then no footer text present | Optional | - |
| footerTextColor | Footer text color in HEX format | Optional | Black color |
| footerHeaderText | Footer header text displayed above footer text | Required | - |
| optOutButtonText | Opt-out button text/title, if not passed then opt-out will be hidden. | Optional | - |
| optOutButtonTextColor | Opt-out button text color in HEX format | Optional | Black color|
| disclosureText | Disclosure text, displayed below the Yes and No buttons and replacing the opt-out button. If “optOutButtonText” is provided then this value will be ignored. | Optional | - |
| disclosureTextColor | Disclosure text color in HEX format | Required | Black color|


<br/>

---
### Custom Prompt Design:

<table><tr><td>
    <img src=Images/CustomPrompt.jpg />
</td></tr></table>
