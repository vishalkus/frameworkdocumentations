# Overview


This guide outlines how to use the MaritzCX Mobile SDK, for both the Android and iOS platforms. It allows a survey to be presented to a user at the following times:

* When a user logs in.
* When a user logs out.
* When a user visits a screen.
* When a user visits a screen and does nothing.
* When a user exits a screen four times.



**Notes:** 

1. This list is not exhaustive. 
2. There is an administrative tool used by our Implementation team members to define, for a given program, which surveys will be presented at which times.

> For each of these scenarios, an event alias must be defined, such as *userLoggedIn*, *userLoggedOut*, etc. Within a given program, there may be many surveys. These rules are definable at the survey level.

> The developer must include the respective SDK into their pre-existing app. The SDK provides a configurable object and method call to reach into the SDK to present a survey (or not), depending upon the rules defined in the aforementioned administration interface. The developer must inform the SDK of the following values, so that the rules can be executed properly:


* **Locale (e.g. en-us, fr-ca, etc.)** - This will be determined automatically if the developer has not indicated it.
* **Program Token** - This is given to the developer by the business owner or manager.
* **Event Alias** - This is the required alias for informing the SDK of which rules to execute. This is given to the developer by the business owner or manager once triggers confirmed.
* **Custom Prompt** – This is required, if developer want display custom prompt instead of default pop-up/prompt. If not passed, then SDK shows default pop-up/prompt.
* **Pre-Population Values** – This is optional field and this need to pass in {key: value} format.


The SDK supports the conditional presentation of a survey. It will also support passing forward to the next screen or remaining on the existing screen. This capability is essential for when an event is triggered that the user expects to produce a result. When the survey is completed and the user exits it, they may proceed as expected.

It is critical that the event aliases are shared and coordinated.


## Mobile SDK Workflow Chart

The following chart illustrates the workflow/process involved in building a client app using the MaritzCX Mobile SDK:

![](Images/Flowchart.jpg)


## Setting Pre-Population Values


Some surveys require that either the survey populate with something (typically about the user) or pass values behind-the- scenes. The SDK requires that you pass these values in by deriving them within the application. This will require that the business owner/manager and the developer agree on the list of named values to be passed in. Then, the developer calls a method for each named value and passes both the name and the value before launching the survey.

<em>Example of pre-population values: </em>

`{"FirstName":"FirstNameValue","LastName":"LastNameValue"}`


## Intended Audience

The intended audience for this document is both the developer who is integrating the SDK into their pre-existing mobile and the business owner or manager who is defining the rules for survey presentation.

<br>
<br>

---

**[iOS Integration](Native_App_Integration_iOS.md)**

**[Android Integration](Native_App_Integration_Android.md)**

**[Custom Prompt](/CustomPrompt.md)**
<br>

---